#!/bin/bash

mysql -u root -p -e "
create user 'hoteldb_client'@'localhost';
create user 'hoteldb_admin'@'localhost' identified by 'qwerty';
grant all on hotel_db.clients to 'hoteldb_admin'@'localhost';
"
