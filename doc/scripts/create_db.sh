#!/bin/bash

mysql -u root -p -e "
create database hotel_db;
use hotel_db;

create table history ( 
       id int not null auto_increment, 
       room_number int not null, 
       client_id int not null, 
       check_in date not null, 
       leaving date not null, 
       primary key (id) 
);

create table rooms (
       room_number int not null, 
       is_free bool not null, 
       client_id int, 
       check_in date, 
       leaving date, 
       max_people_count int not null, 
       cost int unsigned not null, 
       primary key (room_number)
);

create table room_description (
       room_number int not null,
       bar bool,
       view varchar(50),
       comment varchar(400),
       primary key (room_number)
);

create table cleaning_shedule (
       id int not null auto_increment,
       worker_id int not null,
       room_number int not null,
       time_from time not null,
       time_to time not null,
       primary key (id)
);

create table personnel (
       id int not null auto_increment,
       name varchar(100) not null,
       position varchar(100) not null,
       salary int unsigned not null,
       primary key (id)
);

create table clients (
       id int not null auto_increment,
       name varchar(100) not null,
       payment_method int,
       country varchar(30),
       language varchar(30),
       email varchar(100),
       comment varchar(400),
       primary key (id)
);

alter table hotel_db.rooms
add foreign key (client_id)
references hotel_db.clients (id);

alter table hotel_db.room_description 
add foreign key (room_number) 
references hotel_db.rooms (room_number);

alter table hotel_db.cleaning_shedule 
add foreign key (worker_id) 
references hotel_db.personnel (id);

alter table hotel_db.cleaning_shedule 
add foreign key (room_number) 
references hotel_db.rooms (room_number);

alter table hotel_db.history 
add foreign key (room_number) 
references hotel_db.rooms (room_number);

alter table hotel_db.history add 
foreign key (client_id) 
references hotel_db.clients (id);

insert into hotel_db.clients (name, payment_method, country, language, email) values ('Miguel de Cervantes de Saavedra', 0, 'Espana', 'Espanol', 'classic@literature.com');
insert into hotel_db.rooms (room_number, is_free, client_id, max_people_count)  values (101, true, 1, 15);
insert into hotel_db.room_description (room_number, bar, view) values (101, true, 'lake');
insert into hotel_db.personnel (name, position, salary) values ('John Smith', 'cleaner', 50);
insert into hotel_db.cleaning_shedule (worker_id, room_number, time_from, time_to) values (1, 101, '12:00:00', '13:00:00');
insert into hotel_db.history (room_number, client_id, check_in, leaving) values (101, 1, '1560-05-05', '1560-06-05');
"
