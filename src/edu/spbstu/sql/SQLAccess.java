package edu.spbstu.sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javafx.scene.control.TextArea;

public class SQLAccess {
	private Connection connect = null;
	private Statement statement = null;
	private ResultSet resultSet = null;
	private final TextArea textArea; 

	public SQLAccess(TextArea ta) {
		textArea = ta;
	}
	
	public void readDataBase() {
		try {
			// this will load the MySQL driver, each DB has its own driver
			Class.forName("com.mysql.jdbc.Driver");
			// setup the connection with the DB.
			connect = DriverManager
					.getConnection("jdbc:mysql://localhost/hotel_db?"
							+ "user=hoteldb_admin&password=qwerty");

			// statements allow to issue SQL queries to the database
			statement = connect.createStatement();
			// resultSet gets the result of the SQL query
			resultSet = statement
					.executeQuery("select * from hotel_db.clients");
			writeResultSet(resultSet, textArea);

			close();
		} catch (Exception e) {
			e.printStackTrace();			
		}

	}

	private void writeResultSet(ResultSet resultSet, TextArea results) throws SQLException {
		while (resultSet.next()) {
			int id = resultSet.getInt("id");
			String name = resultSet.getString("name");
			String pm = resultSet.getString("payment_method");
			String country = resultSet.getString("country");
			String language = resultSet.getString("language");
			String email = resultSet.getString("email");
			String comment = resultSet.getString("comment");
			results.appendText("|" + id
					+ "|" + name
					+ "|" + pm
					+ "|" + country
					+ "|" + language
					+ "|" + email
					+ "|" + comment
					);
		}
	}

	private void close() {
		try {
			resultSet.close();
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("close error");
		}
		try {
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("close error");
		}
		try {
			connect.close();
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("close error");
		}
	}
}
