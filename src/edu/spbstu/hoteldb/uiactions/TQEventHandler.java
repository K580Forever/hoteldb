package edu.spbstu.hoteldb.uiactions;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextArea;
import edu.spbstu.sql.SQLAccess;

public class TQEventHandler implements EventHandler<ActionEvent>{

	private final TextArea textArea;
	public TQEventHandler(TextArea ta) {
		textArea = ta;
	}
	@Override
	public void handle(ActionEvent arg0) {
		SQLAccess dao = new SQLAccess(textArea);
		dao.readDataBase();
	}

}
