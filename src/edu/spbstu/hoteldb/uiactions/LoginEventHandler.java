package edu.spbstu.hoteldb.uiactions;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import edu.spbstu.hoteldb.Main;

public class LoginEventHandler implements EventHandler<ActionEvent> {

	private final TextField login;
	private final PasswordField pass;
	private final TextArea textArea;
	private final Main main;
	
	public LoginEventHandler(TextField log, PasswordField p, TextArea ta, Main m) {
		login = log;
		pass = p;
		textArea = ta;
		main = m;
	}
	
	@Override
	public void handle(ActionEvent arg0) {
		textArea.appendText("logging in: " + login.getText() + '\n');
		if ((login.getText().compareTo("root") == 0) && 
				(pass.getText().compareTo("qwerty") == 0)) {
			textArea.appendText("log in successfull!\n");
			main.setRootControlPanel();
		} else {
			textArea.appendText("log in failed!\n");
		}
	}

}
