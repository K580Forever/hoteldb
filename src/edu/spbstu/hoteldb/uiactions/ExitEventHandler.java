package edu.spbstu.hoteldb.uiactions;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ExitEventHandler implements EventHandler<ActionEvent> {

	@Override
	public void handle(ActionEvent e) {
		System.exit(0);
	}
	
}
