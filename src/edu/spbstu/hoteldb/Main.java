package edu.spbstu.hoteldb;

import java.awt.Dimension;
import java.awt.Toolkit;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import edu.spbstu.hoteldb.uiactions.ExitEventHandler;
import edu.spbstu.hoteldb.uiactions.LoginEventHandler;
import edu.spbstu.hoteldb.uiactions.TQEventHandler;

public class Main extends Application {

	private TextArea results;
	private HBox content;
	
	public static void main(String[] args) {
		launch(args);
	}
	
	@Override
	public void start(Stage stage) throws Exception {
        stage.setTitle("Hotel_db");
        stage.setMinHeight(500);
        stage.setMinWidth(500);
        stage.setMaximized(true);
        
        results = new TextArea();
        content = new HBox();
        
        VBox root = new VBox();
        content.setSpacing(10);
        content.setPadding(new Insets(10,10,10,10));
        Node cp = createLoginPanel();
        HBox.setHgrow(cp, Priority.ALWAYS);
        root.getChildren().addAll(createMenuBar(), content);
        
        Dimension scrsize = Toolkit.getDefaultToolkit().getScreenSize(); 
        stage.setScene(new Scene(root, scrsize.width*0.6, scrsize.height));
        stage.show();
        content.setPrefSize(stage.getWidth(), stage.getHeight());
        content.getChildren().addAll(cp, createResultsPanel(stage.getWidth(), stage.getHeight()));
	}
	
	private MenuBar createMenuBar() {
		MenuBar menuBar = new MenuBar();
        Menu menuCommon = new Menu("Menu");
        Menu menuOptions = new Menu("Options");
        MenuItem menuItem = new MenuItem("Exit");
        menuItem.setOnAction(new ExitEventHandler());
        menuCommon.getItems().add(menuItem);
        menuBar.getMenus().addAll(menuCommon, menuOptions);
		return menuBar;		
	}
	
	private Node createLoginPanel() {
		VBox vbox = new VBox();
		vbox.setSpacing(10);
		vbox.setPadding(new Insets(10,10,10,10));
		TextField login = new TextField();
		login.setMaxWidth(200);
		login.setPromptText("Login");
		PasswordField pass = new PasswordField();
		pass.setMaxWidth(200);
		pass.setPromptText("Password");
		Button btn = new Button("Log in");
		btn.setOnAction(new LoginEventHandler(login, pass, results, this));
		vbox.getChildren().addAll(login, pass, btn);
		return vbox;
	}
	
	private Node createRootPanel() {
		VBox vbox = new VBox();
		vbox.setSpacing(10);
		vbox.setPadding(new Insets(10,10,10,10));
		Button btn = new Button("Test query");
		btn.setOnAction(new TQEventHandler(results));
		vbox.getChildren().addAll(btn);
		return vbox;
	}
	
	private Node createResultsPanel(double x, double y) {
		VBox vbox = new VBox();
		vbox.setPrefSize(x, y);
		results.setText("Welcome to HotelDB application!\n");
		results.setEditable(false);
		results.setPrefSize(x, y);
		vbox.getChildren().addAll(results);
		return vbox;
	}
	
	public void setRootControlPanel() {
		content.getChildren().remove(0);
		Node rootPanel = createRootPanel();
		HBox.setHgrow(rootPanel, Priority.ALWAYS);
		content.getChildren().add(0, rootPanel);
	}
}
